import requests


def get_installed_collectors(floor_slug, host=r"lab-api.aclima.io", ):
    url = r"http://%s/v1/metadata/installed_collectors?secret=parasschieben&floor_slug=%s&timestamp=2017-06-01 00:00:00" % (host, floor_slug)
    r = requests.get(url)
    return r.json()


if  __name__ == "__main__":
    res = get_installed_collectors(r"us_sf_10-lombard_node-wall-3")
    print res
    print type(res)
